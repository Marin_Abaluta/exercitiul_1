package ro.orangeprojects11;

public class Hotel {

    public int price;
    public String title;
    public String author;

    public Hotel(int price,String title,String author){

        this.price = price;
        this.title = title;
        this.author = author;
    }

    public int getPrice() {
        return price;
    }

    public String display(){

        return "The hotel " + title + " has the owner " + author;
    }

}
